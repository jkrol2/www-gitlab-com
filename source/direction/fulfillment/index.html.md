---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment team at GitLab focuses on creating and supporting the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
Last reviewed: 2022-04
 
## Overview
 
The GitLab Fulfillment section is passionate about creating seamless commerce experiences for our customers. We work across [sales segments](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation), checkout preferences, and hosting options to make it easier for customers to purchase, activate, and manage their GitLab subscriptions. 
 
By making it easier for customers to manage their GitLab subscriptions we increase customer satisfaction and improve our go-to-market (GTM) efficiency. Improving our GTM efficiency helps our team to accelerate revenue growth and helps GitLab scale as a company. 
 
Fulfillment is currently divided into four groups:
- Purchase is responsible for our primary checkout experiences and any product-driven purchase automation
- Provision is responsible for the provisioning of both SaaS and Self-Managed subscriptions. 
- Utilization takes care of usage reporting and usage admin controls. 
- Fulfillment Platform maintains and evolves our underlying order-to-cash infrastructure, including integrations into other systems such as Salesforce and Zuora, to help accelerate our goals as a section. 
 
We collaborate frequently across all of GitLab to achieve our goals. Most commonly we work with [Field Operations](https://about.gitlab.com/handbook/sales/field-operations/), [Enterprise Applications](https://about.gitlab.com/handbook/business-technology/enterprise-applications/), [Growth](https://about.gitlab.com/direction/growth/), and Data teams.
 
If you have any feedback on our direction we'd love to hear from you. Feel free and raise an MR, open an issue, or contact [Omar Fernández](https://gitlab.com/ofernandez2)
 
## Principles
 
Across our stable counterparts, we follow four key principles to keep us focused on delivering the right results. These principles are not absolute, the intent is for them to guide our decision-making.
 
**Make conducting business with GitLab seamless**
 
When customers choose to purchase GitLab they've already decided to unlock additional value by accessing the features or services enabled by a transaction. We strive to make the transaction experiences fade into the background, helping customers unlock this additional value as easily as possible. This creates a better customer experience, as customers don’t want to labor over purchasing, and results in accelerated growth for GitLab.
 
**Build a strong foundation so GitLab can scale**
 
Fulfillment systems are the foundational layer for many commerce activities within GitLab. Our systems provision licenses for customers, are the source of data for multiple KPIs and data models, and interact directly with [Zuora](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/zuora/) and Salesforce. These systems need to be reliable, scale with demand, and allow other teams to collaborate. 
 
**Use data to make decisions and measure impact**
 
We have many sensing mechanisms at our disposal: feedback routed via our GTM teams, meetings with business counterparts, customers feedback on calls, and improvement suggestions raised by GitLab team members and members of the wider community in our issue tracker. 
 
We're also improving how we use data as a sensing mechanism to set direction and prioritization. Understanding our funnel is paramount in building a seamless commerce experience for our customers. Fulfillment teams in collaboration with Growth are instrumenting each point in our transaction funnels so we can use data to inform our strategy and direction.
 
**Iterate, especially when the impact of a change is sizeable**
 
Iteration is one of the most challenging values to follow, especially within Fulfillment. Oftentimes our work needs to be bundled and aligned closely with external announcements or communications. Even so, we strive to break work down as much as possible and decouple functionality releases from broader announcements. Doing this expedites delivering value to our customers and the business.
 
## Vision
 
Our vision is to build seamless commerce experiences for our customers, where each customer can easily buy GitLab paid products and services via their preferred channel and using their preferred payment method.
 
Delivering on this vision requires investments across all interfaces where customers conduct business with GitLab. Given the breadth of countries, organization sizes, and industries that benefit from the GitLab product, we strive to be excellent at both direct transactions via our web commerce portal or our sales team, as well as sales via [Channels and Alliances](https://about.gitlab.com/handbook/sales/#channels--alliances).
 
By providing seamless experiences, our vision is to improve operational efficiency, enabling our Sales teams to spend their time on accounts with [high LAM](https://about.gitlab.com/handbook/sales/sales-term-glossary/#landed-addressable-market-lam), and enable functions like Support and Finance to spend less time manually supporting customers and our field teams.
 
To achieve this vision we are focusing on the following areas: 
 
- Make license, consumption, and user-management fully self-service
- Build a best-in-class webstore
- Drive more transactions to self-service
- Deliver competitive purchasing options in sales-assisted and webstore orders
- Enable channel partners and distributors to deliver experiences as good as our direct sales motions
 
**Make license, consumption, and user-management fully self-service**
 
With GitLab 14.1 we launched [Cloud Licensing](https://docs.gitlab.com/ee/subscriptions/self_managed/#cloud-licensing) this provides a foundation for an improved licensing and provisioning experience for our customers. For years GitLab was behind in modernizing our licensing systems and architecture, building on the foundation of Cloud Licensing we will invest in further reducing friction in license management. To do this, we are investing in two areas: 
 
- **Enterprise License Management:** Licensing and seat management gets more challenging for very large organizations. These large organizations often need to move large amounts of users in and out of GitLab each month, often across multiple instances. We are learning more about these challenges and will deliver solutions to help these customers easily manage provisioning across multiple instances in their organization. The goal is to ensure [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator) and [Skyler (Chief Information Security Officer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#skyler---the-chief-information-security-officer) can easily provision seats to meet the evolving needs of their business.
 
- **Usage and Reporting:** Customers need visibility into usage history and trends so that they can best manage their GitLab paid subscription. This includes visibility into how many licensed seats have been used and any consumption of storage or CI minutes. We will provide solutions to provide this visibility to all customers, including proactive alerting as customers reach their subscription plan limits, to make it easier for customers to predictably manage their GitLab subscription. 
 
**Build a best-in-class webstore**
 
Many new customers first purchase a small subscription or first order. A common example would be [Alex (Application Development Manager)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) choosing to purchase GitLab for their team. Alex may work at a small startup or may be part of a sub-team within a large enterprise. Either way, Alex may start by purchasing GitLab in the webstore as a self-service transaction. At this point, Alex isn't looking to speak to sales as they are looking to make a small investment to get started with GitLab. By building a simple and easy-to-use transaction experience we can get out of Alex's way and get them back to [adopting newly acquired features](https://about.gitlab.com/direction/growth/#drive-feature-adoption-and-usage). We aim to make it extremely simple for Alex to quickly purchase and get back into the GitLab product to develop their applications, using the newly acquired features. 
 
To do this, we're focused first on [moving our purchase experience](https://gitlab.com/groups/gitlab-org/-/epics/1888) from the currently separate website ([customers.gitlab.com](https://customers.gitlab.com/) into the core product. 
 
Other teams at GitLab will contribute to that purchase experience, such as our Growth team who may run experiments to improve the experience and conversion metrics. 
 
We also aim to provide more options at checkout and improve cohesion in our GTM self-service funnel. As one example: we want to enable promotions with discounts from a marketing campaign, making that experience seamless. This will help improve conversion and lead to a better overall experience transacting with GitLab.
 
**Drive more self-service transactions**
 
The focus here is efficiency, we will enable more transactions to be completed via the web store to focus our GTM team on higher-value activities. This may first benefit simpler transactions, but we will invest to support our entire customer base over time. For example, even a large enterprise with a complex purchase may want to, later on, add seats or purchase additional CI minutes during their subscription. The easiest, fastest, and most efficient way to do this would be to allow this digitally and fully self-serve. 
 
As we enable more self-service transactions, we will provide the right visibility into all transactions so that both the customer and our customer-facing teams supporting them are aware of the relevant transaction history.
 
To ensure success in this area we partner closely with our [VP of Online Sales & Self Service](https://about.gitlab.com/job-families/sales/vice-president-online-sales-and-self-service/) and their team. We are working together to map out customer journeys across sales segments, and allow customers to select whether they want to purchase self-service or would benefit from speaking to a sales team member. 
 
**Deliver competitive purchasing options in sales-assisted and webstore orders**
 
The majority of GitLab transactions occur with a credit card and customers pay up-front for their purchase, but that's not how a large part of the world prefers to transact. Outside of the United States, the preferred digital payment method is an e-wallet. In addition, as customers scale their organizations, they have more complex payment and billing requirements. 
 
To meet global demand we will work to support additional payment types, complex billing processes via the webstore (POs, ACH transactions, etc), support multiple currencies, and more.
 
**Enable channel partners and distributors to deliver experiences as good as our direct sales motions**
 
An increasing number of customers begin their GitLab journey via a partner. They may transact in a cloud provider's marketplace or purchase GitLab as part of a software bundle via a distributor. Our goal is to ensure those customers and partners get as high a quality of service as they would buying direct. 
 
This means extending our APIs to support "indirect" transactions and collaborating closely with our counterparts in Channel Ops, Finance, and Enterprise Apps to design solutions that extend our internal systems beyond their direct-sales use-cases. In addition, we plan to invest in additional tooling to give our customers and partners better visibility and flexibility in managing GitLab subscriptions. 
 
<!--- ### Purchase Group ---> 
<!--- Vision to be added ---> 
 
### Provision Group
 
##### Mission
Enable seamless provisioning so that customers can benefit from their paid plans without friction, across both self-managed and SaaS.
 
##### Overview
The Provision Group within Fulfillment enables access to features of any GitLab product tier. We create licensing and provisioning capabilities for all user types. For trial and free users, licenses are provisioned after registration. Paid users are provisioned post initial purchase and at renewal. 
 
We also create tools to help users manage their licenses. For self-managed plan administrators, these tools offer license management capabilities such as activation and license sync. For SaaS, it provides seat management, provisioning, and de-provisioning capabilities. 
 
For GitLab team members, we offer tools for tasks and workflows including the ability to search for a license, generate a license, resend a license key and render views to the customer, license, reconciliation, or activation events.
 
##### What’s next and why
In Aug 2021, we introduced cloud licensing capabilities to improve subscription management and billing. This is now GitLab's default system for managing GitLab licenses. We will continue to strengthen our core capabilities by doing the following:
 
- **Cloud Licensing adoption**
Cloud Licensing adoption aims to increase the percentage of GitLab self-managed instances on paid plans using Cloud Licensing. By FY23 Q2, we are extending cloud licensing to users operating their license in offline mode, giving customers the ability to activate offline but share with GitLab key subscription data via a monthly data export. 
 
- **Internal Efficiency**
We will focus on improving systems and processes that enable us to provision licenses reliably to our customers. This includes improvements to give internal customer-facing team members visibility into licensing status, so they can better support our customers. 
 
- **Vision for the Future of Enterprise License Management at GitLab**
Beginning with FY23 Q2, we will define the vision for license management and provisioning for larger GitLab customers. We will explore opportunities to address customer concerns with applying a license to multiple instances, discover ways to help GitLab reliably track and report against proof of (license) ownership, and investigate an admin system where a licensee can assign, delegate or transfer provisioning to an alternate recipient.
 
<!--- ### Utilization Group ---> 
<!--- Vision to be added ---> 
 
## Group Responsibilities
 
**Purchase**
The Purchase group is responsible for all self-service purchase experiences, supports sales-assisted purchasing, channel and distributor e-marketplaces, subscription management (reconciliation, auto-renewal, invoicing, payment collection, etc), and trials. The group's primary goal is to increase self-service purchase volume to 95.5%.
 
- Creating SKUs
- Purchase flows in GitLab.com
- Purchase flows in the Customer Portal
- Auto-Renewal processes
- Quarterly Subscription Reconciliation processes
- Subscription management features in GitLab.com (invoices, subscription cards, credit cards)
- Emails/In-app notifications related to subscription management
 
**Provision**
The Provision group is responsible for provisioning and managing licenses across self-managed and SaaS (including Cloud License activation/sync and provisioning of legacy licenses).
 
- Post-purchase provisioning and ensuring customers get what they paid for (plan, users, storage, CI minutes, etc.)
- SaaS provisioning
- SM provisioning
 
 
**Utilization**
The Utilization group is responsible for all usage reporting and management, usage admin controls, CI minute management, and storage management.
 
- Features related to visualization of consumables (storage, CI minutes)
- Billable users, max users calculations in SM & SaaS
- Emails/In-app notification related to consumption (users/CI/storage)
- Building admin tools for the support team.
- Create transparency for internal teams in customer subscription, billing, and licensing.
 
 
**Fulfillment Platform**
Newly established in FY23, the Fulfillment Platform team aims to extract the complexity of our underlying billing infrastructure to help other Fulfillment teams build features more quickly. 
 
- Ensuring reliable infrastructure for our Customers Portal
- Consistent data models and integrations across our internal billing systems
- Change Management and SLAs for all Order-to-Cash systems
- Updating downstream sales and marketing systems (SFDC, Marketo)
- Modifying Sentry error logging
 
 
 
## Roadmap
 
Our roadmap is prioritized and scheduled following our [Project management process](/handbook/engineering/development/fulfillment/#project-management-process). We aim to update this roadmap every month as a part of our milestone [planning process](/handbook/engineering/development/fulfillment/#planning). 
 
To request work to be added to the Fulfillment roadmap, please follow our [intake request process](/handbook/engineering/development/fulfillment/#intake-request). Changes in priorities of this roadmap follow our [prioritization process](/handbook/engineering/development/fulfillment/#prioritization).
 
* The source of truth for all Fulfillment projects is our **[internal spreadsheet](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit?usp=sharing)**. We copy over to the handbook only a subset of ongoing initiatives that can be shared publicly. 
* This roadmap was last updated: 2022-04-05
       
| Priority | Initiative                                                           | Team                              |
|----------|----------------------------------------------------------------------|-----------------------------------|
| 1        | Strict Cloud Licensing (Offline Support, Remove License File)        | Purchase, Provision               |
| 2        | Quarterly Subscription Reconciliation (QSR) MVC                      | Purchase |
| 3        | SaaS Free User Efficiency                                            | Purchase, Utilization             |
| 4        | E-Disty Arrow Marketplace Integration                                | Purchase, Provision                 |
| 5       | Improve UX of new sales-assisted GitLab.com subscribers              | Provision                           |
| 6       | Support Admin Tooling                                                | Utilization                           |
{: .table-responsive style="margin-bottom: 25px; white-space: nowrap; display: block;"}
 
### Roadmap Initiatives Prioritization
By nature of our [direction](/direction/fulfillment/), Fulfillment works mostly on highly cross-functional projects where either or both of the following are true:
1. Many cross-team dependencies: project execution relies on collaboration with other teams, with significant coordination of time and resources required.
1. Downstream impacts: projects may change how other teams operate (e.g., Field Operations, Enterprise Apps, Billing) and may also impact the success of their efforts (e.g., achieving sales efficiency targets, accomplishing e-commerce conversion goals)
 
To focus on the most impactful work, Fulfillment’s prioritization process seeks to:
1. Prioritize the highest ROI initiatives as measured by long-term impact on GitLab, Inc’s value. (Note: “cost to build” is a key consideration in the I of the ROI calculation)
1. Provide future visibility into priorities to adequately plan cross-team and cross-functional resource needs.
1. Minimize waste and churn due to re-prioritization mid-execution.
 
#### Prioritization Criteria
A project will be prioritized in the Fulfillment roadmap based on the considerations below.
1. Revenue impact potential
   1. Value of unlocking new sales channels
   1. Sales efficiency improvements
   1. Reduction in time spent by a field team member to close a deal
   1. Eliminating time spent on transactions that sales team members don’t need to work on
   1. Conversion improvements
   1. Risks and compliance issues (negative revenue potential)
1. Operational cost reduction
   1. Reduction in support costs
   1. Seamless transactions with GitLab, efficiency gains, and improved customer satisfaction.
1. Foundations to unlock opportunities
   1. Value of new or improved data to inform future opportunities
   1. System robustness to support 10x customers
   1. Value of the foundational work to unlock other opportunities
   1. Number of GitLab team members able to contribute towards e-commerce improvements
   1. Work that will help us scale (support more customers, improve operations, simplify business processes, etc.)
1. Confidence level around the impact and solution
   1. Low for initiatives that haven’t been properly scoped or researched. PM/UX/Eng and cross-functional partners will increase the confidence by scoping the initiative.
1. Ease of implementation
   1. Consider the time and resources required to complete the initiative.
   1. Consider a solution that is long-term sustainable, and corresponds to the revenue/cost impact estimated.
   1. For efforts that are not well understood, we will start by assuming a larger effort to account for unknowns. As we do some scoping, we can refine the cost/complexity.
 
All initiatives, regardless of who requests them, will be evaluated based on this same criteria.
 
Some initiatives will have a direct impact on these criteria, but others will have an indirect impact. We will consider indirect impact as part of the prioritization.
 
When scoping new solutions we will prefer those that best allow GitLab to scale and accelerate future work. These solutions often require more upfront foundational work, which we will include in the initial scope. In cases when we decide to accelerate a solution by skipping on some foundational work, we will add this foundational work as a separate line item to the roadmap.
 
**A note on Customer Satisfaction**: to understand the impact of efforts aimed at improving customer satisfaction, we should estimate the indirect impact of improving CSAT on revenue and cost. For example, by reducing the number of steps or improving the steps required to purchase we will see an increase in conversion rate and thus revenue.
 
#### Scoring initiatives
 
We use an adapted version of the [RICE Framework](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework). We score from 0 to 5 on each of the prioritization criteria for each initiative added to the [Fulfillment roadmap spreadsheet](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit#gid=1276538540) (Internal only). 
 
The goal of adding scores to each initiative across factors is to:
1. Bring clarity and transparency as to how the product team is thinking about the impact of an initiative relative to others.
1. Help anyone at GitLab provide feedback on mistakes made when assessing the potential of an initiative. Colleagues can point to specific criteria being misevaluated.
 
The final score of an initiative will be determined by a weighted sum of the scores.
1. The criterion scores will be maintained by the Product team, but they will be informed by data and discussions with cross-functional partners.
1. The weighting of each criterion to determine the overall initiative score will depend on company priorities. For example, if a CEO OKR focuses on revenue maximization, we will give more weight to opportunities that unlock net new ARR.
1. The prioritization notes column can elaborate on the rationale behind a score and link to supporting data or information as available.
 
#### Scheduling new work
Prioritization will drive the order in which work is scheduled to be completed, but other considerations such as team capacity or whether the work is parallelizable with other ongoing work will impact when work begins.
 
The product team will review overall prioritization regularly. Before changing priorities will consider:
1. Efficiency and morale impact of disrupting ongoing efforts
1. Impact of changes to existing customer and partner commitments
1. Feedback from cross-functional partners
 
To minimize impact and give more predictability to partner teams, we will minimize changes to initiatives that we’ve already agreed with cross-functional partners to do within the ongoing quarter.
 
Anyone can request new items to be added to the roadmap via an [intake request](/handbook/engineering/development/fulfillment/#intake-request).
 
#### Quarterly cross-functional review
One of our prioritization goals is to maximize overall team output across Fulfillment and cross-functional partners. We want to give transparency to all GitLab team members about the work that Fulfillment and its partner teams plan to deliver.
 
To enable this, we will do a roadmap review with our [stable counterparts](/handbook/engineering/development/fulfillment/#stable-counterparts) before the beginning of a new fiscal quarter. As part of this review, we gather feedback on roadmap priorities, update the roadmap based on the feedback, and agree with partners on the scope and delivery milestones for the upcoming 3-6 months.
 
During these quarterly reviews we will aim to commit up to 70% of Fulfillment’s engineering capacity for the upcoming quarter, and no more than 30% of capacity for the quarter after. This is meant to provide enough visibility into upcoming activities for cross-functional partners to plan for them while leaving room for reprioritization and changes as needed.
 
#### Communicating roadmap changes
Any proposed changes to the roadmap will be first communicated to cross-functional partners async in a relevant Slack channel with the relevant context and rationale, and ask for feedback. As needed, a synchronous meeting will be scheduled to discuss. All feedback will be considered by the product team and a final decision will be made and communicated once made.
 
## OKRs
 
We follow the [OKR (Objective and Key Results)](https://about.gitlab.com/company/okrs/) framework to set and track goals quarterly. The Fulfillment section OKRs are set across the entire [Quad](https://about.gitlab.com/handbook/product/product-processes/#pm-em-ux-and-set-quad-dris):
 
* [FY23-Q1 Fulfillment OKRs](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/382)
* Draft [FY23-Q1 Fulfillment OKRs](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/454)
 
## Performance Indicators
Our performance indicators are [Limited Access](/handbook/communication/#limited-access) and hosted on the following two dashboards:
1. [Executive Dashboard](https://app.periscopedata.com/app/gitlab:safe-dashboard/919356/Supersonics-Executive-Dashboard)
1. [Operational Dashboard](https://app.periscopedata.com/app/gitlab:safe-dashboard/919355/Supersonics-Operational-Dashboard)
