---
layout: handbook-page-toc
title: "Integrated Campaigns"
description: "GitLab Marketing Handbook: Integrated Campaigns"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## Integrated Campaigns Overview
{: #integrated-campaigns .gitlab-purple}

The goal of a GitLab integrated campaign is to strategically land a cohesive message to a target audience across a variety of channels and offers, dependent on the goals of the campaign. Content types include a combination of blog posts, webinars, whitepapers, reports, videos, case studies, and more. Activation of the campaigns takes place in a number of marketing channels include digital ads, social, paid social, SEO, PR, email, and more.

**[Campaign Managers](/handbook/marketing/demand-generation/campaigns/)** are responsible for the strategizing of integrated campaigns and organizing timelines and DRIs for deliverables to execute on the campaigns.

**Questions? Please feel free to ask in the [#marketing-campaigns slack channel](https://gitlab.slack.com/archives/CCWUCP4MS).**

**Questions? Please feel free to ask in the [#marketing-campaigns slack channel](https://gitlab.slack.com/archives/CCWUCP4MS) or ask in the [#marketing slack channel](https://gitlab.slack.com/messages/C0AKZRSQ5) and tag @marketing-campaigns.**

### Campaign Core Teams
{: #campaign-core-teams}

A `Campaign Core Team` is led by a [Marketing Campaign Manager](/handbook/marketing/demand-generation/campaigns/) and is comprised of functional DRIs across marketing and revenue programs.

**Campaign core team members:**
* Campaigns (DRI)
* Product Marketing
* Technical Marketing
* Content Marketing
* Revenue Programs
* Brand
* Corporate Marketing (Liaison for activation)
* Field Marketing (Liaison for activation)

**About Campaign Core Teams:**
- The Core Team's purpose is to collaborate on the research, positioning, messaging, general strategy, and overall timeline of the campaign, and related value plays.
- The Core Teams will determine a relevant and realistic timeline in line with overarching marketing themes, and including Campaigns and Value Plays.
- Each team member should formally commit to due dates for their work.
- Each team member is responsible for communicating as soon as possible when a due date is at risk.
- The Core Teams are comprised of functional DRIs from teams aligned to product specializations (SMEs within their teams)
- The Core Teams will kickoff together with the freedom to drive their campaigns as desired and with a strong team-oriented approach
- The Core Teams will ensure nothing happens in a vacuum, by proactively integrating corresponding activities across marketing, and including those beyond the core team for input
- The Core Teams are responsible for clearly communicating plans to all of Marketing ([also see Core Team communication](/handbook/marketing/campaigns/#core-team-communication))

#### Campaign Core Team Communication
{: #core-team-communication}

How to keep all teams in the loop will be a key responsibility of the Core Teams, and planning should not happen in silos. Continual communication of plans, progress, and how to leverage the GTM Motion collateral will be a key to success in the FY22 Marketing Plan.

The teams are encouraged to contribute ideas and processes for communication here in the handbook throughout FY22.

### What is in a campaign bundle?
{: #campaign-bundles}

Think of a **campaign bundle** as the foundational pieces of an integrated campaign. They are used by all teams in marketing and sales to maintain a cohesive message and branding across all tactics and activities aligned to the topical campaign.

_The list below includes recommended items included in a **Campaign Bundle**, which is developed by the Campaign Core Team consisting of Campaigns, Product Marketing, Technical Marketing, Content Marketing, and Digital Marketing.

To see a visual of campaign bundles, which are then leveraged in activation plans, please see [this MURAL](https://app.mural.co/t/gitlab2474/m/gitlab2474/1619125370999/270c2d5df5a535223c053f2dba0d06833f9b92d1?sender=jgragnola2053).

**What is included in a Campaign Bundle?**
* Target personas
* Persona-based positioning and messaging
* Market landscape
* Product capabilities
* Content journey (including awareness, consideration, and decision stages)
* Analyst relations content (if available)
* Customer references
* Emails for content in buyer journey
* Creative for ads, emails, landing pages
* Alliance partner joint GTM campaigns and activities
* Channel partner "Instant Campaigns"
* Outreach sequences/templates
* SDR enablement

## How are campaign bundles activated?
{: #campaign-activation}

Campaign bundles are leveraged by all teams in marketing and sales to drive a cohesive full-funnel approach with aligned personas, positioning, messaging, and branding across all touchpoints and conversations.

**Activation activites include:**
- Webcasts
- Tech Demos
- Workshops
- Regional events
- Corporate events
- Community events
- Owned events (Commit)

**Activation channels include:**
- Email nurture
- Paid digital (paid social, GDN, etc.)
- Social
- PR
- Community

## How do we measure performance??
{: #campaign-performance}
We leverage multi-touch attribution reporting in Sisense to help us understand which campaigns, activities, and channels drive the best results, with a goal of SAO and Pipeline. We review these metrics at a segment/region breakout level, as well as reviewing overall campaign metrics with the campaign core teams to identify optimization opportunities to maximize results.

Our dashboards are owned and managed by the Marketing Strategy and Performance team. [Learn more in the Bizible Handbook >>](/handbook/marketing/marketing-operations/bizible)

## In Progress and Future Campaigns
{: #upcoming-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

* FY23 Integrated Campaigns are currently in development.
* **[View FY22 Integrated Campaigns and GTM Motions here >>](https://about.gitlab.com/handbook/marketing/plan-fy22/)**
* **[See FY21 Integrated Camapigns here >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/749)**

## Active Integrated Campaigns
{: #active-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

The below global integrated campaigns are continally optimized by the Marketing Campaigns team, in collaboration wit Digtial Marketing, Technical Marketing, Product Marketing, and beyond. These campaign bundles are leveraged by all teams in Marketing to apply cohesive and consistent messaging and approach across all marketing channels and tactics.

### 🚀 CI
{: #ci-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Agnes Oetama**

**Launched: 2020-04-27 and continually optimized**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/741)
* [Solution Resource](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/)
* [Campaign Review 2021-09-09](https://docs.google.com/presentation/d/1TG7SAHUub3BqclJg4nawDARecOOaQ4NpMaxDnXWWtF8/edit#slide=id.g5e411f22b2_0_0) - GitLab Team Members Only
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/873)
* [Live landing page](https://about.gitlab.com/why/use-continuous-integration-to-build-and-test-faster/)
* [Slack](https://gitlab.slack.com/archives/CVDQL20BA)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001df8y)

### 🚀 DevOp Platform
{: #devops-platform}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Indre Kryzeviciene**

**Launched: 2020-04-28 and continually optimized**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1822)
* [Solution Resource](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/)
* [Campaign Review 2021-10-28](https://docs.google.com/presentation/d/1yGif0bLL2xiC_fFUVzM4eSRcU9g-aOkqpPVAElby1KY/edit#slide=id.gaf9c1b1f24_0_0) - GitLab Team Members Only
* [Slack](https://gitlab.slack.com/archives/C01NLEXE34L)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001df8y)

### 🚀 DevSecOps
{: #devsecops-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Agnes Oetama**

**Launched: 2020-04-29 and continually optimized**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/743)
* [Solution Resource](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/)
* [Campaign Review 2021-07-26](https://docs.google.com/presentation/d/1gPMLjIRPNOCt8DqHUWYdwxW2tNH528qg_YbmZaDYzl8/edit#slide=id.g5e411f22b2_0_0) - GitLab Team Members Only
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/874)
* [Live landing page](https://about.gitlab.com/solutions/dev-sec-ops/)
* [Slack](https://gitlab.slack.com/archives/CV8GZ63GR)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001df4w?srPos=0&srKp=701)

### 🚀 GitOps
{: #gitops-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Eirini Pan**

**Launched: 2020-06-22 and continually optimized**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/745)
* [Campaign Review 2021-9-26](https://docs.google.com/presentation/d/1eYF_H95XzrGmgrqbbRy6rYKsqZkpPvCt7sNxyuEsZuk/edit#slide=id.g5e411f22b2_0_0) - GitLab Team Members Only
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/891)
* [Live landing page](https://about.gitlab.com/why/gitops-infrastructure-automation/)
* [Slack](https://gitlab.slack.com/archives/C0119FNPA84)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dgJ9)

### 🚀 Version Control & Collaboration (VC&C)
{: #vcc-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Jenny Tiemann**

**Launched: 2020-06-09 and continually optimized**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/742)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/927)
* [Live landing page](hhttps://about.gitlab.com/why/simplify-collaboration-with-version-control/)
* [Slack](https://gitlab.slack.com/archives/CVB3AKJNA)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfsw)

### 🚀 Competitive campaign 3.0
{: #competitive-campaign-3}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Agnes Oetama**

**Launched (Original): 2020-11-16 and continually optimized**

* [Parent epic with tactical issues](https://gitlab.com/groups/gitlab-com/-/epics/1029)
* [Live landing page - Manager Persona](https://about.gitlab.com/blog/2019/11/26/migrating-from-jenkins/)
* [Live landing page - Practitioner Persona](https://page.gitlab.com/gitlabci-vs-jenkins-demo.html)
* [SFDC Campaign - Manager >>](https://gitlab.my.salesforce.com/7014M000001ll99?srPos=0&srKp=701)
* [SFDC Campaign - Practitioner >>](https://gitlab.my.salesforce.com/7014M000001dlvz?srPos=0&srKp=701)

### 🚀 France CI Localized
{: #ci-france-localized}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Eirini Pan**

**Launched: 2020-05-15**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/752)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/956)
* [Live landing page](https://about.gitlab.com/fr/pourquoi/integration-continue-pour-construire-et-tester-plus-rapidement/)
* [Slack](https://gitlab.slack.com/archives/C012N4QKYQY)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfjQ)

### 🚀 Germany CI Localized
{: #ci-germany-localized}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager DRI: Indre Kryzeviciene**

**Launched: 2020-04-29**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/809)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/982)
* [Live landing page](https://about.gitlab.com/de/warum/nutze-continuous-integration-fuer-schnelleres-bauen-und-testen/)
* [Slack](https://gitlab.slack.com/archives/C012QLG1NJD)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfqW)

## Past Integrated Campaigns
{: #past-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

The below were campaigns run historically, and aligned to overall GTM. They have since been retired in order to run new global campaigns aigned to updated GTM strategies between Sales and Marketing.

### 🌄 Increase operational efficiencies
{: #increase-operational-efficiencies}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jenny Tiemann**

**Launched: 2019-08-23**

* *Note: this is being sunsetted in Q4 FY21 and existing records in nurture will continue until all emails have deployed.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/367)
* [Live landing page](/topics/devops/reduce-devops-costs/)
* [Slack](https://gitlab.slack.com/archives/CCWUCP4MS)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyiL)

### 🌄 Deliver better products faster
{: #deliver-better-products-faster}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Zac Badgley**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/363)
* [Live landing page](/blog/2018/10/12/strategies-to-reduce-cycle-times/)
* [Campaign brief](https://docs.google.com/document/d/1dbEf1YVLPnSpFzSllRE6iNYB-ntjacENRMjUxCt8WFQ/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000Cyhm?srPos=0&srKp=701)

### 🌄 Reduce security and compliance risk
{: #reduce-security-compliance-risk}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Originally Jackie Gragnola, transitioned to Megan Mitchell**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the DevSecOps use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/368)
* [Live landing page](/solutions/dev-sec-ops/)
* [Campaign brief](https://docs.google.com/document/d/1NzFcUg-8c1eoZ1maHHQu9-ABFfQC65ptihx0Mlyd-64/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyeJ)

### 🚀 AWS Partner
{: #aws-partner}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Nout Boctor-Smith**

**Launched: 2019-09-22**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/624)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/638)
* [Live landing page](https://about.gitlab.com/webcast/aws-gitlab-serverless/)
* [Slack](https://gitlab.slack.com/archives/CRTE89C66)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001loj0)

### 🌄 Operation OctoCat
{: #operation-octocat}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jackie Gragnola**

**Launched: 2019-10-31**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/439)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/445)
* [MURAL of campaign user journey](https://app.mural.co/t/gitlab5736/m/gitlab5736/1584992460277/f66e45f8067dded0ae81d711aeb897c1547e2a80)
* [Live landing page](/compare/github-actions-alternative/)
* [Campaign brief](https://docs.google.com/document/d/1Mcy_0cwMsTPIxWUXPgoqw9ejsRJxaZBHi4NikYTabDY/edit#heading=h.kf9lglu57c0t)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lmdK)

### 🌄 CI Build & Test Auto
{: #ci-build-test-auto}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/379)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/432)
* [Live landing page](/resources/ebook-single-app-cicd/)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lkp9)

### 🚀 Competitive campaign
{: #competitive-commit}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched (Original): 2019-04-02**

* [Parent epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/10)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/85)
* [Live landing page](/resources/ebook-single-app-cicd/)
* [SFDC Campaign >>](https://gitlab.my.salesforce.com/70161000000VxvJ)

### 🌄 Just Commit
{: #just-commit}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jackie Gragnola**

**Launched: 2019-02-18**

* [Parent epic with child epics and issues >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/7)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/50)
* [Former landing page (repurposed since close of the campaign)](/blog/2019/03/27/application-modernization-best-practices/)
* [SFDC Campaign](https://gitlab.my.salesforce.com/70161000000VwZq)
* [Meeting recordings >>](https://drive.google.com/drive/u/1/folders/147CtTEPz-fxa0m1bYxZUbOPBik-dkiYV)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1i2OzO13v77ACYo1g-_l3zV4NQ_46GX1z7CNWFsbEPrA/edit#slide=id.g153a2ed090_0_63)

## Campaign Planning
{: #campaign-planning .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

**There will be an epic for every campaign - created by the Campaign Manager managing the campaign.**

* The campaign will have a clear launch date
* All action items will have DRIs and a timeline, working back from the launch date
* The campaign will be determined at least 45 days prior to launch to allow for a proactive, not reactive, timeline

Ideally, campaigns will be agreed at least a quarter in advance to allow for ample planning and execution, prep time with agencies, creative concepting, and communication internally. This is a collaborative effort to deliver a cohesive program.

#### Overall Campaign Steps
{: #campaign-steps}
<!-- DO NOT CHANGE THIS ANCHOR -->

* **Campaign and launch date is determined by Marketing Leadership**
* **Assign:** Campaign Manager is assigned
* **Assign:** Marketing team leaders assign DRIs from their teams
* **Meeting:** Campaign Manager organizes campaign kickoff call with early-stage DRIs (Campaign Manager, DMP, PMM, TMM)
* **Meeting:** Campaign Manager organizes campaign brief call with early-stage DRIs (Campaign Manager, DMP, PMM, TMM)
* **Plan:** Campaign Manager creates project plan (GANTT chart) with timelines and DRIs
* **Plan:** Campaign Manager creates the epic and related issues, including the due dates and DRIs from project plan
* **Meeting:** Campaign Manager organizes the bi-weekly 30 minute check-in call to cover milestones met and determine any blockers/at-risk action items
* **Async:** *DRIs are responsible for delivering by their due dates, with timeline adherence being critical due to dependencies for later tasks to be completed by other teams*
* **Reporting:** Campaign Manager organizes reporting issue with clear DRI to include overall metrics and more detailed drill-in by channel (one month post-launch)
* **Optimization:** Campaign Manager creates issues for optimizing the landing page, channels, etc. and assigning to relevant DRIs

#### Integrated Campaign Meetings
{: #meetings}
<!-- DO NOT CHANGE THIS ANCHOR -->

## Campaign Manager Epic Creation
{: #epic-creation .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Epic Code
{: #epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

***The Campaign Manager of the integrated campaign will create the epic with the following structure - details and links to be added up on creation of documents and timeline defined.***

```
<!-- Name this epic: Name of Campaign [Integrated Campaign] (i.e. Automated Software Delivery [Integrated Campaign])

#### :calendar: [Campaign Execution Timeline >>]() - `to be added once created by Campaign Manager`
#### :mega: [Slack Channel >>]() `to be added once created by Campaign Manager`

## Team
* Campaigns (DRI): [name] @[handle] 
* Product Marketing: 
* Technical Marketing: 
* Content Marketing: 
* Digital Marketing: 
* Brand: 
* Revenue Programs: 
* Partner Marketing: 
* SDR Leadership: 
* SDR Enablement: 
* Field Marketing (Liaison): 
* Corporate Marketing (Liaison): 

## :memo: Campaign Brief
### Business Objectives
`to be added`

### KPIs & Goals
`to be added`

### Target audience and persona
* **Level:** `to be added`
* **Function:** `to be added`

### Marketing message
* Overall: `to be added`
* Key Messages:
- `to be added`
- `to be added`

### Aligned Content & Planned Events
* `To be added`
* `To be added`

### Marketing Channel Mix
`to be added upon discussion with activation teams (digital markeitng, lifecycle marketing, ABM, SDR, etc.)`
* [ ] Email Marketing
* [ ] Paid Ads (SEM)
* [ ] Paid Social
* [ ] Demandbase (Key Accounts)
* [ ] Outreach Emails

## 🔗 UTM for tracking URLs
* Overall utm_campaign - `tbd`, i.e. **`utm_campaign=`** [determine UTM with Campaigns & Digital Marketing]
* [Read more aobut UTM Strategy](https://about.gitlab.com/handbook/marketing/utm-stratey)

## ⚡ Quick Links
* [Marketo Program]()
* [SFDC]()

/label ~"dg-campaigns" ~"mktg-demandgen" ~"Content Marketing" ~"Digital Marketing Programs" ~"Product Marketing" ~mktg-website ~design ~"Social Media" ~"Portfolio Marketing" ~"mktg-status::wip"
```

## Sales Lead Funnel Cross-Over with All-Remote
{: #funnel-crossover-all-remote .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Diagram of sales lead funnel cross-over with all-remote
{: #funnel-crossover-all-remote-diagram}
<!-- DO NOT CHANGE THIS ANCHOR -->

Below is a V1 visual in Mural of the sales lead funnel (pre-opportunity) on the left and the all-remote funnel on the right. Discussion of how and when to "bridge the gap" between the funnels - potentially sharing GitLab audience-targeted offers within All-Remote communications - is included [in this issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2590).

<div style="width: 600px;" class="embed-thumb"> <h1 style="position: relative;vertical-align: middle;display: inline-block; font-size: 24px; line-height:22px; color: #393939;margin-bottom: 10px; font-weight: 300;font-family: Proxima Nova, sans-serif;"> <img src="https://app.mural.co/avatar/jgragnola2053" style="position: absolute; border-radius: 50%; width: 36px;height: 36px;margin-right: 14px; display: inline-block; margin-top: -6px;margin-right: 10px; vertical-align: middle;"> <div style="padding-left:50px"> <span style="max-width:555px;display: inline-block;overflow: hidden; white-space: nowrap;text-overflow: ellipsis;line-height: 1; height: 25px; margin-top: -3px;">Lead Generation Funnel + All-Remote Breakout</span> <span style="position:relative;top:-3px;font-size: 16px; margin-top: -6px; line-height: 24px;color: #393939; font-weight: 300;"> by Jackie Gragnola</span> </div> </h1> <div style="position: relative; height: 0;overflow: hidden; height: 400px; max-width: 800px; min-width: 320px; border-width: 1px; border-style: solid; border-color: #d8d8d8;"> <div style="position: absolute;top: 0;left: 0;z-index: 10; width: 600px; height: 100%;background: url(https://murally.blob.core.windows.net/thumbnails/gitlab24743204/murals/gitlab24743204.1585283889844-5e7d833115ecf04b79301159.png?v=51ddc0c0-b456-443c-aa7c-92557e78efe9) no-repeat center center; background-size: cover;"> <div style="position: absolute;top: 0;left: 0;z-index: 20;width: 100%; height: 100%;background-color: white;-webkit-filter: opacity(.4);"> </div> <a href="https://app.mural.co/t/gitlab24743204/m/gitlab24743204/1585283889844/5837d15d7eed38c848745c9e63cef77eba194009" target="_blank" rel="noopener noreferrer" style="transform: translate(-50%, -50%);top: 50%;left: 50%; position: absolute; z-index: 30; border: none; display: block; height: 50px; background: transparent;"> <img src="https://app.mural.co/static/images/btn-enter-mural.svg" alt="ENTER THE MURAL" width="233" height="50"> </a> </div> </div> <p style="margin-top: 10px;margin-bottom: 60px;line-height: 24px; font-size: 16px;font-family: Proxima Nova, sans-serif;font-weight: 400; color: #888888;"> You will be able to edit this mural. </p></div>
