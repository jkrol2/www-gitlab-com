---
layout: handbook-page-toc
title: Automated Software Delivery POV Scope and Acceptance
description: Automated Software Delivery POV Scope and Acceptance
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices/) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays/) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources/) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development/) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations/) - [**Processes**](/handbook/customer-success/solutions-architects/processes/)  - [**POV**](/handbook/customer-success/solutions-architects/tools-and-resources/pov/)

## Automated Software Delivery POV Scope and Acceptance
{:.no_toc}
The Automated Software Delivery solution is identified for organizations  who are trying to "shift left" to find security vulnerabilities earlier within their DevOps methodology but have not been able to achieve expected results.

From the early discovery when qualifying the POV, it is confirmed that the existing application security is separated from the DevOps flow, causing bottle necks as the final hurdle in the development life cycle and typically controlled only by the App Sec team. 

The key objective of the POV is to validate the value of the shift left security to the developer's hand along with app sec team collaboration at time of app development. 

### Input to the POV

- Pain points identified: Toolchain review and mapped out existing app sec tools and usage 
- High level ROI discussion identifying potential tool consolidation (hard cost); and overall efficiency gains with transformative process with shift left security
- Both DevOps + App sec team are stakeholders to be involved

### Suggested Success Criteria
- Business Driver: increase velocity, consolidate/reduce spending, improve security posture, protect brand and reputation 
- Enterprise Initiative and Sponsor: cloud transformation, CIO
- Required capabilities with the objectives to infuse security earlier in the development process, ability to scan all code and act them real time. Have the security oversight as the integral part of the end to end DevOps.

### Other POV Scope and Acceptance

SA working with SAL and AE can define the POV scope with the customer, with alignment to the business values and the GitLab solution. For each solution, the typical scope and acceptances are listed for reference but the team should define the scope, time and execution with acceptance for each engagement.

- [DevSecOps](/handbook/customer-success/solutions-architects/tools-and-resources/pov/devsecops/)
- [Software Compliance](/handbook/customer-success/solutions-architects/tools-and-resources/pov/compliance/)
- [DevOps Platform](/handbook/customer-success/solutions-architects/tools-and-resources/pov/platform/)




