---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Upcoming Webinars


### April 2022

We’d like to invite you to our free upcoming webinars during the month of April.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### Intro to GitLab
#### April 7th, 2022 at 8:00-9:00 AM Pacific Time/3:00-4:00 PM UTC

For any new users in your organization, this webinar will help guide them on GitLab best practices and answer common adoption questions.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_PcGL-v9jQumVi9JLke5bJQ)

### Intro to CI/CD
#### April 19th, 2022 at 8:00-9:00 AM Pacific Time/3:00-4:00 PM UTC

For anyone new to GitLab CI/CD, we’ll cover a general overview, how to get started with your first CI/CD pipeline in GitLab, and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__sw2MzX0RPKVa5CskNDacQ)

### Advanced CI/CD
#### April 21st, 2022 at 8:00-9:00 AM Pacific Time/3:00-4:00 PM UTC

This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_huetUFsHQEuQS6AAPbwiuA)

### DevSecOps & Compliance 
#### April 26th, 2022 at 8:00-9:00 AM Pacific Time/3:00-4:00 PM UTC

We’ll cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_rZfoHvOySTe8YA0etieoZw)
